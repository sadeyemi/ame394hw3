/*
    Seni Adeyemi
    AME 394
    Fall 2015

    Assignment 2 details:

    Create a one page html application that evaluates mathematical equations (Simple calculator that can handle +‐"*"/() and ^). Along with the result, visually depict each step showing
        1. The postfix expression being parsed (current character being scanned).
        2. Contents of the stack
        3. The result evaluated so far.
*/

function calculatePostfix()
{
  var infix = document.getElementById("inputEq").value;
  var tokens = [];
  for (var i = 0; i < infix.length; i++){
    if(infix[i] != " "){
      tokens[tokens.length] = infix[i];
    }
  }
  getPostfix(tokens);

  eval(getPostfix(tokens));



}

var stackObj = { // Create stack object
  content: [], // Content attribute to hold operands and operators in array
  push: function(val){
      stackObj.content[stackObj.content.length] = val;
  },
  pop: function(){
    var top = stackObj.getTop();
    if(top!=null){
      stackObj.content = stackObj.content.splice(0,stackObj.content.length -1);
    }
    return top;
  },
  getTop: function(){
    if(stackObj.content.length == 0){
      return null;
    }
    return stackObj.content[stackObj.content.length-1];
  },
  initialize: function(initStack)
  {
    stackObj.content = initStack;
  }
}

function updateVisualization(step, tokens, postfix)
{
  var outS = "<hr><h1>Step " + (step + 1) + "</h1>";
  //update infix
  outS += "<div>";
  var infix = document.getElementById("inputEq").value;
  outS += "<h2>Infix:</h2>";
  for (var i = 0; i < tokens.length; i++){
    if( i == step ){
      outS += "<span class='smallBox'><b>" + tokens[i] + "</b></span>";
    }
    else{
      outS += "<span class='smallBox'>" + tokens[i] + "</span>";
    }
  }
  outS += "</div>";

  //update stack
  tokens = stackObj.content;
  outS += "<div>";
  outS += "<h2>Stack:</h2>";
  for (var i = 0; i < tokens.length; i++){
    outS += "<span class='smallBox'>" + tokens[i] + "</span>";
  }
  outS += "<span class='smallBox'><b>&lt;</b></span>";
  outS += "</div>";

  //update postfix string
  tokens = postfix;
  outS += "<div>";
  outS += "<h2>Postfix:</h2>";
  for (var i = 0; i < tokens.length; i++){
    if( i == step ){
      outS += "<span class='smallBox'><b>" + tokens[i] + "</b></span>";
    }
    else{
      outS += "<span class='smallBox'>" + tokens[i] + "</span>";
    }
  }
  outS += "</div>";
  document.getElementById("visual").innerHTML += outS;
}

var operators = ["*", "/", "+", "-", "^", "(", ")"];
function isOperator(t)
{
  if(operators.indexOf(t) >= 0){
    return true;
  }
  return false;
}

function isPrecGreater(a, b)
{
  var pn1 = 1;
  var pn2 = 1;
  if(a == "^"){
    pn1 = 3;
  }
  if(b == "^"){
    pn2 = 3;
  }
  if(a == "*" || a == "/"){
    pn1 = 2;
  }
  if(b == "*" || b == "/"){
    pn2 = 2;
  }
  console.log(pn1, pn2);
  if(pn1 >= pn2){
    return true;
  }
  return false;
}

function getPostfix(tokens)
{
  var postFix = "";
  stackObj.initialize([]);
  document.getElementById("visual").innerHTML = "";
  for (var i = 0; i < tokens.length; i++){
    var currScan = tokens[i];
    console.log(currScan);
    if(isOperator(currScan)){ // is operator
      var top = stackObj.getTop();
      if(top != null){  // stack not empty
        while(top && isPrecGreater(top, currScan)){
          postFix += stackObj.pop();
          top = stackObj.getTop();
        }
        stackObj.push(currScan);
      }
      else{  //stack is empty
        stackObj.push(currScan);
      }
    }
    else{ // is NOT an operator
      postFix = postFix + currScan;
    }
    updateVisualization(i, tokens, postFix);
  }

  var top = stackObj.getTop();
  if(top != null){
    document.getElementById("visual").innerHTML += "<h1> Poping stack....</h1>";
    while(top){
      postFix += stackObj.pop();
      top = stackObj.getTop();
    }

  }


    updateVisualization(i, tokens, postFix);

  document.getElementById("visual").innerHTML += "<h1> Result: " + postFix + "</h1><hr>";


  return postFix;
}

function evalutatePostfix(postfixVal){
    stackObj.initialize(postfixVal);
    var ch;

    for(var i = 0; i < stackObj.content.length; i++){
        ch = stackObj.content[i];

        if(isOperator(ch) == false){
            stackObj.push(ch);
        }

        else if(isOperator(ch) == true){
            var b = +stackObj.pop();
            var a = +stackObj.pop();

            var value = operatorFunction[ch](a, b);
            stackObj.push(value);
        }
    }

    document.getElementById("visual").innerHTML += "<h1> Value: " + value + "</h1><hr>";
    return value;
}

function eval(string){
    stackObj.initialize([]);
    var resultValue = 0;
    var temp;


    for(i = 0; i < string.length; i++){

        if(isOperator(string[i]) == false){
            stackObj.push(string[i]);
        }
        else if(string[i] == "("){
          if(stackObj.content == [] || stackObj.getTop == "("){
            stackObj.push(string[i]);
          }
        } else if(string[i] == ")"){
          while(stackObj.getTop != "("){
            stackObj.pop;
          }
        }
        else{


            temp = stackObj.getTop();
            stackObj.pop();
            retVal = operatorFunction[string[i]](temp, stackObj.getTop());

            stackObj.push(retVal);
        }
    }

    resultValue = stackObj.getTop();

    document.getElementById("visual").innerHTML += "<h1> Value: " + resultValue + "</h1><hr>";

    return resultValue;
}

//var num = 0;

var operatorFunction = {
  "+": function (a, b) {
    return parseInt(a) + parseInt(b)
    },
  "-": function (a, b) {

    return parseInt(a) - parseInt(b)
    },
  "*": function (a, b) {

    return parseInt(a) * parseInt(b)
    },
  "/": function (a, b) {

    return parseInt(a) / parseInt(b)
    },
  "^": function (a, b){
    return Math.pow(b, a)
  }
};

function clear()
{
  document.getElementById("visual").innerHTML = "";
  document.getElementById("inputEq").value = "";
}
